// upnp test program

#include  <iostream> 
#include "upnpbase.h"
#include <string.h> 
#include <stdlib.h>




using namespace std;

int main(int argc, char *argv[])
{

	CUPnPControlPoint*	m_upnp;
	std::vector<CUPnPPortMapping> m_upnpMappings;

	int InternalPort, ExternalPort;

	if(argc != 6)
	{
		cout<<"port [-a/-d] [external port] [internal port] [TCP/UDP] [Desctription] "<<endl;
		return 0;
	}

	if(strcmp(argv[1], "-a") && strcmp(argv[1], "-d"))
	{
		cout<<"-a : add an port forwarding enty"<<endl;
		cout<<"-d : delete an port forwarding enty"<<endl;		
		cout<<argv[1]<<endl;
		return 0;
	}

	ExternalPort = atoi(argv[2]);
	if(ExternalPort < 1024 || ExternalPort > 65536)
	{
		cout<<"[external port] should between 1024~65536"<<endl;
		cout<<ExternalPort<<endl;
		return 0;
	}		
	
	InternalPort = atoi(argv[3]);
	if(InternalPort < 1024 || InternalPort > 65536)
	{
		cout<<"[internal port] should between 1024~65536"<<endl;
		cout<<InternalPort<<endl;
		return 0;
	}		

	if(strcmp(argv[4], "TCP") && strcmp(argv[4], "UDP"))
	{
		cout<<"[TCP/UDP] -protocol shout be TCP or UDP"<<endl;
		cout<<argv[4]<<endl;
		return 0;
	}

	if(strlen(argv[5]) > 20)
	{
		cout<<"description length should be less than 20 characters"<<endl;
		cout<<argv[5]<<endl;
		return 0;
	}
    m_upnpMappings.resize(1);
	m_upnpMappings[0] = CUPnPPortMapping(
                        ExternalPort,
                        InternalPort,
                        argv[4],
                        true,
                        argv[5]);
    
	m_upnp = new CUPnPControlPoint(5667);

	if(!strcmp(argv[1], "-a"))
		m_upnp->AddPortMappings(m_upnpMappings);	 
	if(!strcmp(argv[1], "-d"))
	    m_upnp->DeletePortMappings(m_upnpMappings);
    cout<<"ok"<<endl;
    return 0;
}
