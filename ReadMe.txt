src/  This directory contains source code.
bin/  This directory contains compiled executable file and library. 
   portforward is executable file and should be put into /opt/bin/
   libupnp.so, libupnp.so.2, libupnp.so.2.0.1 are dependent libraries. Please put them to /usr/lib/

Usage��

root@nvc1ED0:/mnt/nfs# ./portforward
port [-a/-d] [external port] [internal port] [TCP/UDP] [Desctription]

root@nvc1ED0:/mnt/nfs# ./portforward -a 4568 4125 TCP aadsf
192.168.1.208
ok

root@nvc1ED0:/mnt/nfs# ./portforward -d 2111 2222 TCP aadsf
ok